package com.beagleinteractive.kingengine;


import static com.beagleinteractive.kingengine.KEngine.ke;


/* (c) BeagleInteractive 2019
KingEngine v 1.0

----------------------------------------------------------------------------------------------------
Part of KEngine
----------------------------------------------------------------------------------------------------
 */
public final class KTimerJob {
    // ---------------------------------------------------------------------------------------------
    // VARS
    // ---------------------------------------------------------------------------------------------
    boolean
            destroyed,
            infinite;

    float   recTime,
            delayInSec,
            elapsedTime;

    IKRoutine routine;


    // ---------------------------------------------------------------------------------------------
    // API
    // ---------------------------------------------------------------------------------------------


    /**
     * CTR
     * @param routine
     * @param delayInSec
     * @param infinite
     */
    KTimerJob(IKRoutine routine, float delayInSec, boolean infinite) {
        this.routine = routine;
        this.delayInSec = delayInSec;
        this.infinite = infinite;

        reset();
    }

    KTimerJob(IKRoutine routine, float delayInSec) {
        this(routine, delayInSec, false);
    }


    /**
     * Get record time
     * @return
     */
    public final float getRecTime() { return recTime; }


    /**
     * Get the delay in seconds
     * @return
     */
    public final float getDelayInSec() { return delayInSec; }


    /**
     * Get the elapsed time
     * @return
     */
    public final float getElapsedTime() { return elapsedTime; }


    /**
     * Step the job
     * @return
     */
    public final boolean step()
    {
        // micro optimization for eval
        //if(frameCount % 2 == 0)
        //{
        if(ke.time >= elapsedTime)
            return false;
        //}

        return true;
    }


    /**
     * Reset the job
     */
    public final void reset() {
        this.elapsedTime = ke.time + delayInSec;
        this.recTime = ke.time;
    }


    /**
     * Get if this job is ciclic
     * @return
     */
    public final boolean isInfinite() { return infinite; }
}
