package com.beagleinteractive.kingengine;


import java.util.ArrayList;


/* (c) BeagleInteractive 2019
KingEngine v 1.0

----------------------------------------------------------------------------------------------------
Part of KEngine
----------------------------------------------------------------------------------------------------
*/
public abstract class KEntity implements IKComponent {
    // ---------------------------------------------------------------------------------------------
    // VARS
    // ---------------------------------------------------------------------------------------------
    public ArrayList<IKComponent> components;

    public boolean  visible=true,
                    enabled=true;

    //public String tag;
    //public int layer;

    private boolean _init,
                    _destroyed;

    private KScene _scene;

    //private long _handle;


    // ---------------------------------------------------------------------------------------------
    // API
    // ---------------------------------------------------------------------------------------------


    /**
     * CTR
     * @param scene
     */
    public KEntity(KScene scene)
    {
        _scene = scene;

        //_handle = ++_gHandle;
        components = new ArrayList<IKComponent>();

        if(scene != null) {
            scene.toAddEntities.add(this);
            scene.addPending = true;
        }
    }


    /**
     * Gets the container scene
     * @return
     */
    public KScene getScene() { return _scene; }


    /**
     * Initialize this entity
     * Causes the onInit callback
     */
    public final void init()
    {
        if(_init || _destroyed)
            return;

        onInit();

        _init=true;
    }


    public final void update()
    {
        if(!enabled || _destroyed)
            return;

        init();
        for(IKComponent c : components)
            c.init();

        onUpdate();

        for(IKComponent c : components)
            c.update();
    }


    public final void render()
    {
        if(!visible || _destroyed)
            return;

        onRender();

        for(IKComponent c : components)
            c.render();
    }


    public final void destroy()
    {
        if(_destroyed)
            return;

        _scene.toRemoveEntities.add(this);
        _scene.removePending =true; // causes regen

        onDestroy();

        _destroyed=true;

        for(IKComponent c : components)
            c.destroy();
    }


    public abstract void onInit();
    public abstract void onUpdate();
    public abstract void onRender();
    public abstract void onDestroy();
}
