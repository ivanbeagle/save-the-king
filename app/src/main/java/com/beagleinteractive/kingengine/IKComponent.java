package com.beagleinteractive.kingengine;


public interface IKComponent {
    void init();
    void update();
    void render();
    void destroy();
}
