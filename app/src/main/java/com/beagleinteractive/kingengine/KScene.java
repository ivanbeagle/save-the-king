package com.beagleinteractive.kingengine;


import java.util.ArrayList;
import static com.beagleinteractive.kingengine.KEngine.ke;


/* (c) BeagleInteractive 2019
KingEngine v 1.0

----------------------------------------------------------------------------------------------------
Part of KEngine
----------------------------------------------------------------------------------------------------
*/
public class KScene {
    // ---------------------------------------------------------------------------------------------
    // VARS
    // ---------------------------------------------------------------------------------------------
    ArrayList<KEntity>
            entities,
            toRemoveEntities,
            toAddEntities;

    boolean removePending,
            addPending;


    private boolean _init,
                    _updateLoop,
                    _renderLoop;

    private float _startTime;


    // ---------------------------------------------------------------------------------------------
    // API
    // ---------------------------------------------------------------------------------------------


    /**
     * CTR
     * @param engine
     */
    public KScene()
    {
        entities = new ArrayList<KEntity>();
        toRemoveEntities = new ArrayList<KEntity>();
        toAddEntities = new ArrayList<KEntity>();
    }


    /**
     * Gets the scene running time from its start
     * @return
     */
    public final float getSceneTime() { return ke.time - _startTime; }


    /**
     * Override this
     */
    public final void update()
    {
        if(!_init) {
            _startTime = ke.time;
            onInit();
        }

        _init=true;

        /*if(isPause)
          return;*/

        _updateLoop=true;
        for(KEntity e : entities)
            e.update();
        _updateLoop=false;

        onUpdate();

        // safe add or remove
        checkPendings();
    }


    /**
     * Override this
     */
    public final void render()
    {
        onPreRender();

        _renderLoop=true;
        for(KEntity e : entities)
            e.render();
        _renderLoop=false;

        onPostRender();
    }


    /**
     * Performs pending operations
     */
    final void checkPendings() {
        // optimize this iteraction with a flag
        if(removePending) {
            removePending = false;

            for(KEntity e : toRemoveEntities)
                entities.remove(e);

            toRemoveEntities.clear();
        }

        if(addPending) {
            addPending = false;

            for(KEntity e : toAddEntities)
                entities.add(e);

            toAddEntities.clear();
        }
    }


    protected void onPreRender() {}
    protected void onPostRender() {}
    protected void onInit() {}
    protected void onUpdate() {}
}


