package com.beagleinteractive.kingengine;


import android.media.SoundPool;
import android.util.Log;
import java.util.ArrayList;
import processing.core.PApplet;


/* (c) BeagleInteractive 2019
KingEngine v 1.0

----------------------------------------------------------------------------------------------------
CORE Engine
----------------------------------------------------------------------------------------------------
 */
public class KEngine extends PApplet {
    // ---------------------------------------------------------------------------------------------
    // VARS
    // ---------------------------------------------------------------------------------------------
    public static KEngine ke;
    public static SoundPool soundPool;

    float  time,
           deltaTime;

    private ArrayList<KTimerJob> _jobs;
    private KScene _activeScene;


    // ---------------------------------------------------------------------------------------------
    // API
    // ---------------------------------------------------------------------------------------------


    public static float time() { return ke.time; }

    public static float deltatime() {
        return ke.deltaTime;
    }


    /**
     * CTR
     */
    public KEngine() {
        super();

        ke = this;
    }


    public void log(String s) {
        println("LOG: " + s);
        Log.v("KENGINE", s);
    }


    /**
     * Gets time
     * @return
     */
    public final float getTime() { return time; }


    /**
     * Gets the delta time
     * @return
     */
    public final float getDeltaTime() { return deltaTime; }


    /**
     * Init King Engine
     */
    public final void init() {
        time = 0;
        _jobs = new ArrayList<KTimerJob>();
    }


    /**
     * Run King Engine in game loop
     */
    public final void run(boolean stepTime)
    {
        deltaTime = 0;

        if(stepTime) {
            // calc delta time and global time
            deltaTime = (float)1 / frameRate;
            time += deltaTime;
        }

        // check timers job to run and pop
        ArrayList<KTimerJob> toRemove=new ArrayList<KTimerJob>();

        for(KTimerJob t : _jobs) {
            if(t.destroyed)
            {
                toRemove.add(t);
                continue;
            }

            if(!t.step())
            {
                // collect invalid timer
                if(!t.isInfinite())
                    toRemove.add(t);

                // run action
                t.routine.doJob(null);
                // reset if next use is allowed else it will never be restarted
                t.reset();
            }
        }

        // clean
        for(KTimerJob t2 : toRemove)
            _jobs.remove(t2);

        _activeScene.update();
        _activeScene.render();
    }


    /**
     * Free King Engine
     */
    public final void free() {
        soundPool.release();
        soundPool = null;
        System.gc();
    }


    /**
     * Sets the active scene
     * @param scene
     */
    public final void setScene(KScene scene) {
        _activeScene = null;
        //System.gc();
        _activeScene = scene;
    }


    /**
     * Run a KRoutine
     * @param routine
     * @param delayInSec
     * @param infinite
     * @return
     */
    public final KTimerJob startJob(IKRoutine routine, float delayInSec, boolean infinite)
    {
        // create job
        KTimerJob job = new KTimerJob(routine, delayInSec, infinite);
        _jobs.add(job);

        return job;
    }


    /**
     * Destroy pending jobs
     * WARNING: Don't call inside a job!
     */
    public final void destroyPendingJobs() {
        for(KTimerJob t : _jobs) {
            t.destroyed=true;
        }
    }


    public void loadResources() {}
    public void settings() {}
    public void setup() {}
    public void draw() {}
}