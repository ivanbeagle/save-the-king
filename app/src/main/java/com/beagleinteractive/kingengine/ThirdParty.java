package com.beagleinteractive.kingengine;


import processing.core.PGraphics;
import processing.core.PImage;
import processing.core.PShape;
import processing.core.PVector;
import static com.beagleinteractive.kingengine.KEngine.ke;


/* (c) BeagleInteractive 2019
KingEngine v 1.0

----------------------------------------------------------------------------------------------------
Part of KEngine, third party code & extensions
----------------------------------------------------------------------------------------------------
*/
public final class ThirdParty {
    // ---------------------------------------------------------------------------------------------
    // VARS
    // ---------------------------------------------------------------------------------------------
    public static final int Y_AXIS = 0;
    public static final int X_AXIS = 0;




    // ---------------------------------------------------------------------------------------------
    // ImageSequence CLASS
    // ---------------------------------------------------------------------------------------------
    public static final class ImageSequence {
        // -----------------------------------------------------------------------------------------
        // VARS
        // -----------------------------------------------------------------------------------------
        public PImage[] images;
        public int imagesCount;


        // -----------------------------------------------------------------------------------------
        // API
        // -----------------------------------------------------------------------------------------


        /**
         * CTR
         * @param imagePrefix
         * @param ext
         * @param numberFormat
         * @param count
         * @param startIdx
         */
        public ImageSequence(String imagePrefix, String ext, int numberFormat, int count, int startIdx) {
            imagesCount = count;
            images = new PImage[imagesCount];

            for (int i = 0; i < imagesCount; i++) {
                String filename = imagePrefix + ke.nf(i+startIdx, numberFormat) + ext;
                images[i] = ke.loadImage(filename);

                //LOG("loaded image sequence["+i+"]: " + filename + " = " + images[i]);
            }
        }


        /**
         * Gets the width
         * @return
         */
        public int getWidth() {
            return images[0].width;
        }


        /**
         * Gets the height
         * @return
         */
        public int getHeight() {
            return images[0].height;
        }
    }


    // ---------------------------------------------------------------------------------------------
    // GIFAnimationEvt CLASS
    // ---------------------------------------------------------------------------------------------
    public static final class GIFAnimationEvt {
        // -----------------------------------------------------------------------------------------
        // VARS
        // -----------------------------------------------------------------------------------------
        public GIFAnimation animation;
        public int frame;


        // -----------------------------------------------------------------------------------------
        // API
        // -----------------------------------------------------------------------------------------


        /**
         * CTR
         * @param anim
         * @param f
         */
        public GIFAnimationEvt(GIFAnimation anim, int f) { animation = anim; frame = f; }
    }


    // ---------------------------------------------------------------------------------------------
    // GIFAnimation CLASS
    // ---------------------------------------------------------------------------------------------
    public static final class GIFAnimation {
        // -----------------------------------------------------------------------------------------
        // VARS
        // -----------------------------------------------------------------------------------------
        public ImageSequence sequence;
        public IKRoutine frameEvent;
        public int frame, tintCol = -1;
        public boolean play=true, reversed = false;
        public float delay, angle;
        public PVector position, scale;

        private float _lastDelay;


        /**
         * CTR
         * @param sequence
         */
        public GIFAnimation(ImageSequence sequence)
        {
            this.sequence = sequence;
            frame = 0;

            position = new PVector(0,0);
            scale = new PVector(1,1);
            angle = 0;
        }


        /**
         * Render this animation
         * @param x
         * @param y
         * @param w
         * @param h
         */
        public void render(int x, int y, int w, int h) {
            if(tintCol != -1)
                ke.tint(tintCol);

            if(play)
            {
                float d = ke.time;
                if((d - _lastDelay > delay))
                {
                    if(!reversed)
                        frame = (frame+1) % sequence.imagesCount;
                    else {
                        frame -= 1;
                        if(frame < 0)
                            frame = sequence.imagesCount-1;
                    }

                    _lastDelay = d;

                    if(frameEvent!=null)
                        frameEvent.doJob(new GIFAnimationEvt(this, frame));
                }
            }

            ke.pushMatrix();
            mtransform(position, scale, angle);
            if(w != 0 && h != 0)
                ke.image(sequence.images[frame], x, y, w, h); // put at 0,0
            else
                ke.image(sequence.images[frame], x, y); // put at 0,0
            ke.popMatrix();

            ke.noTint();
        }

        public void render() {
            render(0,0,0,0);
        }
    }




    // ---------------------------------------------------------------------------------------------
    // VARIOUS UTILS
    // ---------------------------------------------------------------------------------------------




    // https://processing.org/examples/lineargradient.html
    public static void linearGradient(int x, int y, float w, float h, int c1, int c2, int axis, PGraphics image) {
        image.noFill();

        if (axis == Y_AXIS) {  // Top to bottom gradient
            for (int i = y; i <= y+h; i++) {
                float inter = ke.map(i, y, y+h, 0, 1);
                int c = ke.lerpColor(c1, c2, inter);
                image.stroke(c);
                image.line(x, i, x+w, i);
            }
        }
        else if (axis == X_AXIS) {  // Left to right gradient
            for (int i = x; i <= x+w; i++) {
                float inter = ke.map(i, x, x+w, 0, 1);
                int c = ke.lerpColor(c1, c2, inter);
                image.stroke(c);
                image.line(i, y, i, y+h);
            }
        }
    }


    public static void linearGradient(int x, int y, float w, float h, int c1, int c2, int axis) {
        ke.noFill();

        if (axis == Y_AXIS) {  // Top to bottom gradient
            for (int i = y; i <= y+h; i++) {
                float inter = ke.map(i, y, y+h, 0, 1);
                int c = ke.lerpColor(c1, c2, inter);
                ke.stroke(c);
                ke.line(x, i, x+w, i);
            }
        }
        else if (axis == X_AXIS) {  // Left to right gradient
            for (int i = x; i <= x+w; i++) {
                float inter = ke.map(i, x, x+w, 0, 1);
                int c = ke.lerpColor(c1, c2, inter);
                ke.stroke(c);
                ke.line(i, y, i, y+h);
            }
        }
    }


    public static void drawText(String txt1, int col1, String txt2, int col2, int x, int y) {
        ke.fill(col1);
        ke.text(txt1, x, y);
        ke.fill(col2);
        ke.text(txt2, x+ke.textWidth(txt1), y);
    }


    public static void drawTextShadow(String txt, int col, int shadowCol, int x, int y, int offX, int offY) {
        ke.fill(shadowCol);
        ke.text(txt, x+offX, y+offY);
        ke.fill(col);
        ke.text(txt, x, y);
    }


    public static boolean hitRect(float x1, float y1, float w1, float h1, float x2, float y2, float w2, float h2) {

        // are the sides of one rectangle touching the other?

        if (x1 + w1 >= x2 &&    // r1 right edge past r2 left
                x1 <= x2 + w2 &&    // r1 left edge past r2 right
                y1 + h1 >= y2 &&    // r1 top edge past r2 bottom
                y1 <= y2 + h2) {    // r1 bottom edge past r2 top
            return true;
        }
        return false;
    }


    // CREDITS: https://processing.org/examples/circlecollision.html
    public static boolean hitCircle(float x1, float y1, float r1, float x2, float y2, float r2) {
        // get distances between the balls components
        PVector distanceVect = PVector.sub(new PVector(x2,y2), new PVector(x1,y1));

        // calculate magnitude of the vector separating the balls
        float distanceVectMag = distanceVect.mag();

        // minimum distance before they are touching
        float minDistance = r1 + r2;

        if (distanceVectMag < minDistance)
            return true;

        return false;
    }


    // CREDITS: http://www.jeffreythompson.org/collision-detection/poly-poly.php
    // POLYGON/POLYGON
    public static boolean polyInPoly(PVector[] p1, PVector[] p2) {

        // go through each of the vertices, plus the next
        // vertex in the list
        int next = 0;
        for (int current=0; current<p1.length; current++) {

            // get next vertex in list
            // if we've hit the end, wrap around to 0
            next = current+1;
            if (next == p1.length) next = 0;

            // get the PVectors at our current position
            // this makes our if statement a little cleaner
            PVector vc = p1[current];    // c for "current"
            PVector vn = p1[next];       // n for "next"

            // now we can use these two points (a line) to compare
            // to the other polygon's vertices using polyLine()
            boolean collision = lineInPoly(p2, vc.x,vc.y,vn.x,vn.y);
            if (collision) return true;

            // optional: check if the 2nd polygon is INSIDE the first
            collision = pointInPoly(p1, p2[0].x, p2[0].y);
            if (collision) return true;
        }

        return false;
    }


    // POLYGON/LINE
    public static boolean lineInPoly(PVector[] vertices, float x1, float y1, float x2, float y2) {

        // go through each of the vertices, plus the next
        // vertex in the list
        int next = 0;
        for (int current=0; current<vertices.length; current++) {

            // get next vertex in list
            // if we've hit the end, wrap around to 0
            next = current+1;
            if (next == vertices.length) next = 0;

            // get the PVectors at our current position
            // extract X/Y coordinates from each
            float x3 = vertices[current].x;
            float y3 = vertices[current].y;
            float x4 = vertices[next].x;
            float y4 = vertices[next].y;

            // do a Line/Line comparison
            // if true, return 'true' immediately and
            // stop testing (faster)
            boolean hit = lineInLine(x1, y1, x2, y2, x3, y3, x4, y4);
            if (hit) {
                return true;
            }
        }

        // never got a hit
        return false;
    }


    // LINE/LINE
    public static boolean lineInLine(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4) {

        // calculate the direction of the lines
        float uA = ((x4-x3)*(y1-y3) - (y4-y3)*(x1-x3)) / ((y4-y3)*(x2-x1) - (x4-x3)*(y2-y1));
        float uB = ((x2-x1)*(y1-y3) - (y2-y1)*(x1-x3)) / ((y4-y3)*(x2-x1) - (x4-x3)*(y2-y1));

        // if uA and uB are between 0-1, lines are colliding
        if (uA >= 0 && uA <= 1 && uB >= 0 && uB <= 1) {
            return true;
        }
        return false;
    }


    // POLYGON/POINT
    // used only to check if the second polygon is
    // INSIDE the first
    public static boolean pointInPoly(PVector[] vertices, float px, float py) {
        boolean collision = false;

        // go through each of the vertices, plus the next
        // vertex in the list
        int next = 0;
        for (int current=0; current<vertices.length; current++) {

            // get next vertex in list
            // if we've hit the end, wrap around to 0
            next = current+1;
            if (next == vertices.length) next = 0;

            // get the PVectors at our current position
            // this makes our if statement a little cleaner
            PVector vc = vertices[current];    // c for "current"
            PVector vn = vertices[next];       // n for "next"

            // compare position, flip 'collision' variable
            // back and forth
            if (((vc.y > py && vn.y < py) || (vc.y < py && vn.y > py)) &&
                    (px < (vn.x-vc.x)*(py-vc.y) / (vn.y-vc.y)+vc.x)) {
                collision = !collision;
            }
        }
        return collision;
    }


    // CREDITS: https://www.openprocessing.org/sketch/135314/
    public static PVector lineItersection(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4) {
        float bx = x2 - x1;
        float by = y2 - y1;
        float dx = x4 - x3;
        float dy = y4 - y3;

        float b_dot_d_perp = bx * dy - by * dx;

        if(b_dot_d_perp == 0) return null;

        float cx = x3 - x1;
        float cy = y3 - y1;

        float t = (cx * dy - cy * dx) / b_dot_d_perp;
        if(t < 0 || t > 1) return null;

        float u = (cx * by - cy * bx) / b_dot_d_perp;
        if(u < 0 || u > 1) return null;

        return new PVector(x1+t*bx, y1+t*by);
    }


    // CREDITS: https://forum.processing.org/one/topic/how-do-i-find-if-a-point-is-inside-a-complex-polygon.html
    public static boolean pointInPolygon(PVector pos, PVector[] verts) {
        int i, j;
        boolean c=false;
        int sides = verts.length;
        for (i=0,j=sides-1;i<sides;j=i++) {
            if (( ((verts[i].y <= pos.y) && (pos.y < verts[j].y)) || ((verts[j].y <= pos.y) && (pos.y < verts[i].y))) &&
                    (pos.x < (verts[j].x - verts[i].x) * (pos.y - verts[i].y) / (verts[j].y - verts[i].y) + verts[i].x)) {
                c = !c;
            }
        }
        return c;
    }


    public static void mtransform(PVector p, PVector s, float r)
    {
        ke.scale(s.x, s.y);
        ke.translate(p.x, p.y);
        ke.rotate(r);
    }


    public static void mtransform(PShape shape, PVector p, PVector s, float r)
    {
        shape.scale(s.x, s.y);
        shape.translate(p.x, p.y);
        shape.rotate(r);
    }
}
