package com.beagleinteractive.kingengine;


import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.SoundPool;
import static com.beagleinteractive.kingengine.KEngine.ke;


/* (c) BeagleInteractive 2019
KingEngine v 1.0

----------------------------------------------------------------------------------------------------
Part of KEngine
----------------------------------------------------------------------------------------------------
*/
public class KSound {
    // ---------------------------------------------------------------------------------------------
    // VARS
    // ---------------------------------------------------------------------------------------------
    private int _soundId=-1;
    private float _volume=0.5f;


    // ---------------------------------------------------------------------------------------------
    // API
    // ---------------------------------------------------------------------------------------------


    /**
     * CTR
     * @param applet
     * @param filename
     */
    public KSound(String filename) {
        _volume=0.5f;

        if(KEngine.soundPool == null) {
            KEngine.soundPool = new SoundPool(5, AudioManager.STREAM_MUSIC,0);
        }

        try
        {
            AssetFileDescriptor afd = ke.getActivity().getAssets().openFd(filename);
            _soundId = KEngine.soundPool.load(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength(), 1);
        }
        catch (Exception ex) {
            ke.println(ex.getMessage());
            ex.printStackTrace();
            _soundId = -1;
        }
    }


    /**
     * Get the sound vol
     * @return
     */
    public float getVolume() { return _volume; }


    /**
     * Set the sound vol
     * @param v
     */
    public void setVolume(float v) { amp(v); _volume = v; }


    /**
     * Fade out sound and stop it
     * @param speed
     */
    public void fadeOut(float speed) {
        if (_volume <= 0)
            return;

        _volume -= speed;

        if (_volume <= 0) {
            _volume = 0;
            stop();
            return;
        }

        amp(_volume);
    }


    /**
     * Play the sound
     */
    public void play()  { KEngine.soundPool.play(_soundId, _volume, _volume,0,0, 1); }


    /**
     * Pause the sound
     */
    public void pause() { KEngine.soundPool.pause(_soundId); }


    /**
     * Resume the sound
     */
    public void resume() { KEngine.soundPool.resume(_soundId); }


    /**
     * Repeat sound n times, if -1 is infinite
     * @param times
     */
    public void loop(int times) { KEngine.soundPool.setLoop(_soundId, times);}


    /**
     * Stop the sound
     */
    public  void stop() {
        KEngine.soundPool.stop(_soundId);
    }


    // ---------------------------------------------------------------------------------------------
    // code
    // ---------------------------------------------------------------------------------------------


    private void amp(float f) {
        KEngine.soundPool.setVolume(_soundId, f, f);
    }
}
