package com.beagleinteractive.kingengine;


public interface IKRoutine<T> {
    void doJob(T in);
}
