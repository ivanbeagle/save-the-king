package com.beagleinteractive.savetheking;


import android.content.res.Resources;
import com.beagleinteractive.kingengine.KEngine;
import com.beagleinteractive.kingengine.KSound;
import processing.core.PFont;
import processing.core.PGraphics;
import processing.core.PImage;
import processing.data.JSONObject;
import static com.beagleinteractive.kingengine.ThirdParty.*;


public class GameFragment extends KEngine {
    // ---------------------------------------------------------------------------------------------
    // VARS
    // ---------------------------------------------------------------------------------------------
    // const
    public static final int BANNER_HEIGHT = 60,
                            MAX_ENEMIES = 8,
                            B=1, C=2, D=3, E=4, F=5, G=6, H=7,
                            X=0, Y=1;

    // sys
    public static int   F_SER_VER = 2,
                        DISPLAY_W,
                        HALF_DISPLAY_W,
                        DISPLAY_H,
                        HALF_DISPLAY_H,
                        GAMEVIEW_H = 0,
                        MAX_LEVEL = 16,
                        ROWS = 12;

    public static float SQUARE_SIZE,
                        HALF_SQUARE_SIZE,
                        DIFFICULT_MULT = 85;

    // game <S = serializable>
    public static int   level = 0,
                        score = 0,
                        localRecord = 0, // S
                        playTimes=0; // S

    public static boolean   reminderTutorial=true, // S
                            stepGame = true,
                            isGameOver = false,
                            isPlaying = false;

    public static JSONObject json = null;

    // tut
    public static boolean tutorial = true;
    public static GIFAnimation arrow;

    public static PImage    hand,
                            boxhelp,
                            gradmon,
                            menu,
                            gameOver,
                            record,
                            poweroo,
                            wK, wp, bp, bN, bB, bR, bQ;

    public static PGraphics checkboard;
    public static PFont font;

    // sounds
    public static KSound
            soundGameOver,
            soundPower,
            soundQueenAdv,
            soundSpawner,
            soundExplo;


    // ---------------------------------------------------------------------------------------------
    // API
    // ---------------------------------------------------------------------------------------------


    public static int getPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }


    public static float[] grid2world(int x, int y) {
        y += 1; // offset from caption(score & level)
        return new float[]{ (x)*SQUARE_SIZE , (ROWS-y)*SQUARE_SIZE};
    }


    public static int[] world2grid(float x, float y) {
        int gx, gy;

        if(x == 0)
            gx = 0;
        else
            gx = parseInt(x/SQUARE_SIZE);

        if(y == 0)
            gy = 0;
        else
            gy = (ROWS-1) - parseInt(y/SQUARE_SIZE);

        return new int[]{ gx,gy };
    }


    // ---------------------------------------------------------------------------------------------
    // code
    // ---------------------------------------------------------------------------------------------


    @Override
    public void loadResources() {
        log("Load resources...");

        // loading resources
        menu = loadImage("MainMenu.png");
        gameOver = loadImage("GameOver.png");
        record = loadImage("record.png");
        boxhelp = requestImage("boxhelp.png");
        gradmon = requestImage("gradmon.png");
        poweroo = requestImage("powerup_oo.png");
        hand = requestImage("cursor.png");

        wK = requestImage("wK.png");
        wp = requestImage("wp.png");
        bp = requestImage("bp.png");
        bN = requestImage("bN.png");
        bB = requestImage("bB.png");
        bR = requestImage("bR.png");
        bQ = requestImage("bQ.png");

        ImageSequence seq = new ImageSequence("arrow_",".png", 1, 4, 0);
        arrow = new GIFAnimation(seq);
        arrow.delay=0.3f;

        font = createFont("font.ttf",SQUARE_SIZE);
        textFont(font);

        soundGameOver = new KSound("gameover.mp3");
        soundPower = new KSound("power.mp3");
        soundExplo = new KSound("explosion.mp3");
        soundQueenAdv = new KSound("queenAdvisor.mp3");
        soundSpawner = new KSound("spawner.mp3");

        // load config
        try
        {
            json = loadJSONObject("savetheking.json");

            // read version
            int fVersion = json.getInt("fVersion", F_SER_VER);
            if(fVersion != F_SER_VER)
            {
                // RESET
                //localRecord=0;
                playTimes=0;
                reminderTutorial=true;
            }
            else {
                localRecord = json.getInt("score", 0);
                playTimes = json.getInt("playTimes", 0);
                reminderTutorial = json.getBoolean("reminderTutorial", true);
            }

            tutorial = reminderTutorial;
        }
        catch(Exception ex) {

            json = new JSONObject();

            localRecord=0;
            playTimes=0;
            reminderTutorial=true;

        }

        // put version
        json.setInt("fVersion",F_SER_VER);
    }


    @Override
    public void settings() {
        log("Init settings...");

        //noSmooth();

        DISPLAY_W = displayWidth;
        DISPLAY_H = displayHeight - getPx(BANNER_HEIGHT);

        DIFFICULT_MULT = 95;
        MAX_LEVEL = 21;
        //}

        // calc square
        SQUARE_SIZE = DISPLAY_W / (float)8;
        HALF_SQUARE_SIZE = SQUARE_SIZE / 2;

        // calc client
        GAMEVIEW_H = parseInt(SQUARE_SIZE * ROWS);

        size(DISPLAY_W, DISPLAY_H, P3D); // P3D

        HALF_DISPLAY_W = DISPLAY_W/2;
        HALF_DISPLAY_H = DISPLAY_H/2;
    }


    @Override
    public void setup() {
        log("Save The King ver 1.2");
        log("Init setup...");

        try {
            hint(DISABLE_TEXTURE_MIPMAPS);
            hint(DISABLE_DEPTH_MASK);
            textMode(SHAPE);
        } catch (Exception ex) {
            log("some GL error:");
            println(ex.getMessage());
            ex.printStackTrace();
        }

        orientation(PORTRAIT);

        loadResources();

        background(0);

        //noLoop();
        //redraw();

        try
        {
            init();

            setScene(new MenuScene());
        }
        catch(Exception ex) {
            println(ex.getMessage());
            ex.printStackTrace();

            exit();
        }
    }


    @Override
    public void draw()
    {
        //clear();

        //if(isPlaying) {
        //pushMatrix();
        //scale(0.95f, 0.95f, 0);
        //translate(((DISPLAY_W*0.05f)/2),0,0);
        //}

        run(stepGame);

        //if(isPlaying)
        //popMatrix();
    }
}
