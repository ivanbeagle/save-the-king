package com.beagleinteractive.savetheking;


import com.beagleinteractive.kingengine.KEntity;
import com.beagleinteractive.kingengine.KScene;
import static com.beagleinteractive.savetheking.GameFragment.*;


public abstract class GridObj extends KEntity {
    // ---------------------------------------------------------------------------------------------
    // VARS
    // ---------------------------------------------------------------------------------------------
    public float x, y; // pos
    public float targetx, targety; // target

    boolean die;
    int directionSign = 1;


    // ---------------------------------------------------------------------------------------------
    // API
    // ---------------------------------------------------------------------------------------------


    /**
     * CTR
     * @param scene
     */
    protected GridObj(KScene scene) {
        super(scene);
    }


    public boolean isDirty() {
        return die || y > GAMEVIEW_H - SQUARE_SIZE;
    }


    public void setByGrid(int x, int y) {
        float[] gv = grid2world(x,y);
        this.x = gv[0];
        this.y = gv[1];
    }


    public void onUpdate() {
        if(stepGame && !isGameOver)
            nextMove();
    }


    public void onInit() { }


    public abstract boolean collide(GridObj other);
    public abstract void onDestroy();

    protected abstract void nextMove();
}
