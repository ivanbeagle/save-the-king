package com.beagleinteractive.savetheking;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import androidx.appcompat.app.AppCompatActivity;
import processing.android.PFragment;
import static com.beagleinteractive.savetheking.GameFragment.*;


public class GameActivity extends AppCompatActivity {
    // ---------------------------------------------------------------------------------------------
    // VARS
    // ---------------------------------------------------------------------------------------------
    static GameActivity instance;

    GameFragment game;

    protected PFragment fragment;

    // ---------------------------------------------------------------------------------------------
    // code
    // ---------------------------------------------------------------------------------------------


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        instance = this;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        View frame = findViewById(R.id.gameView);

        game = new GameFragment();

        fragment = new PFragment(game);
        fragment.setView(frame, this);

        //try {
            AdMobIntegration.init(this);
        //}
        /*catch (Exception ex) {
            println(ex.getMessage());
            ex.printStackTrace();
        }*/
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (game != null) {
            game.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onNewIntent(Intent intent) {
        if (game != null) {
            game.onNewIntent(intent);
        }

        super.onNewIntent(intent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (game != null) {
            game.onActivityResult(requestCode, resultCode, data);
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        /*if(AdMobIntegration.isFullAdShow())
            return;*/

        if(game.isGameOver)
        {
            game.setScene(new MenuScene());
            return;
        }

        moveTaskToBack(true);

        super.onPause();
    }

    @Override
    public void onResume() {
        //fragment.onResume();
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        game.free();

        super.onDestroy();
    }
}
