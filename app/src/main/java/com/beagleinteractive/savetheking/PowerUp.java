package com.beagleinteractive.savetheking;


import com.beagleinteractive.kingengine.KScene;
import static com.beagleinteractive.kingengine.ThirdParty.*;
import static com.beagleinteractive.savetheking.GameFragment.*;


public class PowerUp extends GridObj
{
    // ---------------------------------------------------------------------------------------------
    // VARS
    // ---------------------------------------------------------------------------------------------
    public float w, h; // size

    private float _fade = 255;
    private GameScene _scene;


    // ---------------------------------------------------------------------------------------------
    // API
    // ---------------------------------------------------------------------------------------------


    /**
     * CTR
     * @param scene
     */
    public PowerUp(KScene scene) {
        super(scene);
        w=(SQUARE_SIZE - HALF_SQUARE_SIZE);
        w+=sqrt(w);
        h=w;

        _scene = (GameScene)scene;
    }


    public boolean collide(GridObj other) {
        return hitRect(this.x, this.y, w, h, other.x+10, other.y+10, SQUARE_SIZE-20, SQUARE_SIZE-20);
    }


    public boolean isDirty() {
        return y > GAMEVIEW_H - SQUARE_SIZE || _fade <= 0;
    }


    public void nextMove() {
        if(_scene.castling)
            die=true;

        if(die)
        {
            _fade-=2;
            _fade = constrain(_fade,0,255);
            y+=2;
            return;
        }

        // common calcs
        float power = deltatime() * level * (DIFFICULT_MULT); // 65~/70% power
        float min, max;

        max = H * SQUARE_SIZE + (w/2+w/4);
        min = 0;
        float t = time();
        x+=((power/3) * (1+abs(cos(t)))) * directionSign;
        y+=((power/6)) * (1+ (2 * abs(sin(t))));

        if(x >= max) {
            x = max;
            directionSign *= -1;
        } else if(x < min) {
            x = min;
            directionSign *= -1;
        }
    }


    public void onDestroy() {
        //LOG("adios power up!");
    }


    public void onRender() {
        float s = sin(time());
        float c = (200*s)+55;
        ke.tint(255,255,c,_fade);
        int fx = parseInt(255-_fade)/20;
        ke.image(poweroo, x-fx,y-fx, w+fx*2,h+fx*2);
        ke.noTint();
    }
}