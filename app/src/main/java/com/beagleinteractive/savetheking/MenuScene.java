package com.beagleinteractive.savetheking;


import com.beagleinteractive.kingengine.IKRoutine;
import com.beagleinteractive.kingengine.KScene;
import static com.beagleinteractive.kingengine.ThirdParty.*;
import static com.beagleinteractive.savetheking.GameFragment.*;


public class MenuScene extends KScene
{
    // ---------------------------------------------------------------------------------------------
    // VARS
    // ---------------------------------------------------------------------------------------------
    private boolean _tap;


    // ---------------------------------------------------------------------------------------------
    // API
    // ---------------------------------------------------------------------------------------------


    /**
     * CTR
     * @throws Exception
     */
    public MenuScene()
    {
        super();
        isGameOver = false;
        stepGame = true;
    }


    public void onInit() {
        ke.log("a game of Beagle Interactive...");

        _tap = false;

        isGameOver = false;
        stepGame = true;
        isPlaying = false;
    }


    public void onUpdate() {
        float t = getSceneTime();

        if(ke.mousePressed && t > 0.25f)
        {
            if(_tap)
                return;

            _tap=true;

            ke.startJob(new IKRoutine() {
                @Override
                public void doJob(Object in) {
                    try
                    {
                        playTimes++;

                        if(playTimes % 4 == 0 || BuildConfig.DEBUG) {

                            GameActivity.instance.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if(AdMobIntegration.adFull != null && AdMobIntegration.adFull.isLoaded())
                                    {
                                        AdMobIntegration.adFull.show();
                                    } else
                                        ke.setScene(new GameScene());
                                }
                            });
                        }
                        else
                            ke.setScene(new GameScene());
                    }
                    catch(Exception ex)
                    {
                        ke.println(ex.getMessage());
                        ex.printStackTrace();

                        ke.setScene(new GameScene());
                    }
                }
            }, 1,false);
        }
    }


    public void onPostRender() {
        float t = getSceneTime();

        if(_tap)
            renderLoading(t);
        else
            renderMenu(t);
    }


    private void renderLoading(float t) {
        ke.background(0);

        String loading = "L O A D I N G";

        if(t-((int)t) > 0.25f)
            drawTextShadow(loading, 150, 50,  parseInt(HALF_DISPLAY_W - ke.textWidth(loading)/2), parseInt(HALF_DISPLAY_H), 1, 3);
    }


    private void renderMenu(float t) {
        //ke.background(0);
        ke.image(menu, 0, 0, DISPLAY_W, DISPLAY_H);

        ke.textSize(SQUARE_SIZE);

        if(t-((int)t) > 0.50f)
            drawTextShadow("TAP TO PLAY", 255, 100,  parseInt(HALF_DISPLAY_W - ke.textWidth("TAP TO PLAY")/2), parseInt(HALF_DISPLAY_H + SQUARE_SIZE*2), 0, 2);

        drawTextShadow(""+localRecord, ke.color(0,80,0), 10,  parseInt(HALF_DISPLAY_W - ke.textWidth(""+localRecord)/2), parseInt(DISPLAY_H - (HALF_SQUARE_SIZE+(HALF_SQUARE_SIZE/2))), 1, 3);
    }
}
