package com.beagleinteractive.savetheking;


import com.beagleinteractive.kingengine.KScene;
import com.beagleinteractive.kingengine.KTimerJob;
import processing.core.PApplet;
import static com.beagleinteractive.kingengine.ThirdParty.*;
import static com.beagleinteractive.savetheking.GameFragment.*;


public class Piece extends GridObj {
    enum PieceType {
        KING,
        PAWN, KNIGHT, BISHOP, ROOK, QUEEN
    }


    // ---------------------------------------------------------------------------------------------
    // VARS
    // ---------------------------------------------------------------------------------------------
    public boolean isEnemy = true;
    public PieceType type = PieceType.PAWN; // 0: pawn, 1: knight, 2: bishop, 3: rook, 4: queen
    public float delay;

    boolean magnify;

    //private Stack<PVector> _particles = null;
    private PieceType _displayType = PieceType.PAWN;


    // ---------------------------------------------------------------------------------------------
    // API
    // ---------------------------------------------------------------------------------------------


    /**
     * CTR
     * @param scene
     * @param type
     */
    public Piece(KScene scene, PieceType type) {
        super(scene);
        this.type = type; // logical type
        this._displayType = type; // ex: queen can move as all pieces but is displayed as queen!

        // an exact action for the kind of piece!
        /*if(type == PieceType.BISHOP)
            _particles = new Stack<PVector>();*/

        if(type == PieceType.PAWN)
            targety = y + SQUARE_SIZE;

        else if(type == PieceType.ROOK)
            RooksMap.ensure(this);
        //...

        if(x > HALF_DISPLAY_W)
            directionSign = -1;
    }


    public boolean collide(GridObj other) {
        //int offset = 0, square_offset = 0;
        //offset = parseInt(HALF_SQUARE_SIZE / 1.8f);
        //square_offset = offset*2;

        float r = HALF_SQUARE_SIZE/8 + HALF_SQUARE_SIZE / 2;

        return hitCircle(
                this.x + HALF_SQUARE_SIZE,
                this.y + HALF_SQUARE_SIZE,
                r,
                other.x + HALF_SQUARE_SIZE,
                other.y + HALF_SQUARE_SIZE,
                r);

    /*return hitRect(
            this.x + offset, this.y + offset, SQUARE_SIZE - square_offset, SQUARE_SIZE - square_offset,
            other.x + offset, other.y + offset, SQUARE_SIZE - square_offset, SQUARE_SIZE - square_offset
    );*/
    }


    public void onDestroy() {
        if(type == PieceType.ROOK)
            RooksMap.remove(this);

        if(die || !isDirty())
            new Explosion(this.getScene(), x,y, isEnemy ? ke.color(0,0,0,255) : ke.color(255,255,255,255), false, null);
    }


    // event of home reached, useful for queen or special conditions
    private void onHomeReached() {
        // randomize the queen
        if(_displayType == PieceType.QUEEN || type == PieceType.QUEEN)
        {
            _displayType = PieceType.QUEEN;
            int k = (int)ke.random(1,5);
            type = PieceType.values()[k];
            targetx=0;
            targety=0;

            if(type == PieceType.PAWN)
                targety = y + SQUARE_SIZE;

            // prevent that in case the rook queen the row is the same of the king!
            if(type == PieceType.ROOK) {
                int[] gridPos = world2grid(x,y);
                if(gridPos[Y] == 2) // king
                {
                    onHomeReached();
                }
            }
        } // endif
        //else if(_displayType == PieceType.KNIGHT) {
      /*if(!soundTic.isPlaying()) {
        soundTic.setVolume(0.2f);
        soundTic.play();
      }*/
        //}
    }


    public void nextMove() {
        // block for non enemy pieces
        if(!isEnemy)
            return;

        float dt = ke.getDeltaTime();

        // small delay
        delay = constrain(delay - dt, 0, 2);
        if(delay > 0)
            return;

        // common calcs
        float power = dt * level * (DIFFICULT_MULT + (_displayType == PieceType.QUEEN ? 10 : 0)); // 65~/70% power
        float distx=0, disty=0, min, max;
        float snap = HALF_SQUARE_SIZE/20;
        float lerpPower = constrain((power/10)/power,0,1);
        int[] gridPos;


        switch(type) {
            case KING:
                // king is moveable only by user!
                return;

            case QUEEN:
                // first check
                //LOG("first queen check");
                onHomeReached();
                return;

            case PAWN:
                // pawn has only a direction
                disty = abs(dist(0,y,0,targety));
                y = lerp(y, targety, lerpPower);
                if(disty <= snap) {
                    // snap
                    y = targety;
                    targety=y+SQUARE_SIZE;
                    delay = 0.3f;

                    onHomeReached();
                }

                return;

            // **********************************************************************************
            // most difficult!!! can randomize orientation every time
            case KNIGHT:

                //lerpPower = lerpPower / 2 + lerpPower / 4;

                magnify=true;
                if(targetx != 0 || targety != 0) {
                    distx = abs(dist(x,0,targetx,0));
                    disty = abs(dist(0,y,0,targety));
                }

                // if can follow the target on x
                // x has precedence!
                if(targetx != 0)
                {
                    x = lerp(x, targetx, lerpPower);
                    if(distx <= snap) {
                        // snap
                        x = targetx;
                        targetx=0;

                        // randomize next time or no
                        if(ke.random(1,10) > 5)
                            directionSign *= -1;

                        // event
                        //onHomeReached();
                    }
                }

                else if(targety != 0)
                {
                    y = lerp(y, targety, lerpPower);
                    if(disty <= snap) {
                        // snap
                        y = targety;
                        targety=0;

                        // randomize next time or no
                        if(ke.random(1,10) > 5)
                            directionSign *= -1;

                        // invalidate the target
                        targetx=0;

                        magnify = false;
                        delay = 0.2f;

                        // event
                        onHomeReached();

                        return;
                    }
                }

                // detect target check
                else if(targetx == 0)
                {
                    // calc new L
                    gridPos = world2grid(x,y);
                    int col = gridPos[X];
                    int row = gridPos[Y];

                    int columnDestLong = col - 2;
                    int columnDestShort = col - 1;
                    boolean canMoveLong = columnDestLong >= 0;
                    boolean canMoveShort = columnDestShort >= 0;

                    if(directionSign > 0)
                    {
                        columnDestLong = col + 2;
                        columnDestShort = col + 1;
                        canMoveLong = columnDestLong <= H;
                        canMoveShort = columnDestShort <= H;
                    }

                    // in the cases that i can't move on both ways invert the direction
                    if(!canMoveLong && !canMoveShort) {
                        directionSign *= -1; // INVERT
                    }
                    else
                    {
                        float[] destGridLong = grid2world(columnDestLong,row-1); //
                        float[] destGridShort = grid2world(columnDestShort,row-2); // L

                        if(canMoveLong && canMoveShort)
                        {
                            int rand = PApplet.parseInt(ke.random(0,10));
                            if(rand > 5) {
                                targetx = destGridLong[X];
                                targety = destGridLong[Y];
                            }
                            else {
                                targetx = destGridShort[X];
                                targety = destGridShort[Y];
                            }
                        }
                        else if(canMoveLong && !canMoveShort) {
                            targetx = destGridLong[X];
                            targety = destGridLong[Y];
                        } else if(!canMoveLong && canMoveShort) {
                            targetx = destGridShort[X];
                            targety = destGridShort[Y];
                        }
                    } // endelse
                } // endif targetx
                return;

            // **********************************************************************************
            case BISHOP:
                max = H*SQUARE_SIZE;
                min = 0;
                x+=(power/1.8) * directionSign;
                y+=(power/1.8);

                boolean queen = (_displayType == PieceType.QUEEN);

                if(x > max) {
                    x = max;
                    y = grid2world(0, world2grid(0,y)[Y])[Y]; // patch to snap
                    directionSign *= -1;
                    delay = 0.6f;

         /*if(queen)
           y+=power/2;*/

                    onHomeReached();

                    if(queen)
                        return;

                } else if(x < min) {
                    x = min;
                    y = grid2world(0, world2grid(0,y)[Y])[Y];
                    directionSign *= -1;
                    delay = 0.6f;

         /*if(queen)
           y+=power/2;*/

                    onHomeReached();

                    if(queen)
                        return;
                }

                //y+=power/2;

                if(queen)
                    return;

       /*PVector newParticle = new PVector(x+random(HALF_SQUARE_SIZE/2),y);
       int nParticles = _particles.size();
       if(nParticles == 0 || newParticle.dist(_particles.lastElement()) > HALF_SQUARE_SIZE) {
         _particles.push(newParticle);
         if(nParticles+1 == 10)
            _particles.removeElementAt(0);
       }*/
                return;

            // **********************************************************************************
            case ROOK:
                distx = abs(dist(x,0,targetx,0));
                disty = abs(dist(0,y,0,targety));

                //lerpPower = lerpPower / 2 + lerpPower / 4;

                // there is a target?
                if(targetx != 0)
                {
                    x = lerp(x, targetx, lerpPower);
                    if(distx <= snap) {
                        // snap
                        x = targetx;
                        targetx=0;
                        targety=(y)+SQUARE_SIZE;
                        if(targety==0)
                            targety = SQUARE_SIZE;

                        directionSign *= -1;
                        delay = 0.2f;
                    }
                }

                else if(targety != 0)
                {
                    y = lerp(y, targety, lerpPower);
                    if(disty <= snap) {
                        // snap
                        y = targety;
                        targety=0;
                        targetx=0;
                        delay = 0.2f;

                        onHomeReached();
                    }
                }

                else if(targetx==0) {
                    ke.randomSeed(ke.millis());

                    // check for the same row of king
                    gridPos = world2grid(x,y);
                    if(gridPos[Y] == 2) // king
                    {
                        //LOG("hey rook, quiet!");
                        min = 4 * SQUARE_SIZE;
                        max = 4 * SQUARE_SIZE;
                    } else {

                        if (level <= 7) {
                            min = (4 - PApplet.parseInt(round(ke.random(2)))) * SQUARE_SIZE;
                            max = (3 + PApplet.parseInt(round(ke.random(2)))) * SQUARE_SIZE;
                        } else {
                            min = (3 - PApplet.parseInt(round(ke.random(1)))) * SQUARE_SIZE;
                            max = (4 + PApplet.parseInt(round(ke.random(1)))) * SQUARE_SIZE;
                        }
                    }

                    if(directionSign > 0)
                        targetx = max;
                    else
                        targetx = min;
                }
                return;

            // **********************************************************************************
        }
    }


    public void onRender() {
        if(y < SQUARE_SIZE)
            return;

        if(isEnemy)
            ke.fill(50,50,50);
        else
            ke.fill(200,200,200);

        switch(_displayType)
        {
            case KING:
                if(isGameOver)
                {
                    magnify = false;
                }

                if(magnify) {
                    float offs=SQUARE_SIZE/8;
                    ke.image(wK, x-offs, y-offs, SQUARE_SIZE+(offs*2), SQUARE_SIZE+(offs*2));
                } else {
                    if(!isGameOver) {
                        ke.image(wK, x, y, SQUARE_SIZE, SQUARE_SIZE);
                    }
                    else
                    {
                        KTimerJob timer = ((GameScene)this.getScene()).getDieTimer();
                        float timeDiff = (timer.getElapsedTime() - ke.getTime());
                        float fade = (timeDiff / timer.getDelayInSec()) * 255;

                        ke.pushMatrix();
                        ke.translate(x+HALF_SQUARE_SIZE,y+HALF_SQUARE_SIZE);
                        ke.rotate(radians(fade * 4));
                        ke.imageMode(CENTER);

                        ke.tint(ke.color(255,255,255,parseInt(fade)));
                        ke.image(wK,0,0,SQUARE_SIZE,SQUARE_SIZE);
                        ke.noTint();

                        ke.popMatrix();
                        ke.imageMode(CORNER);
                    }
                }
                break;

            case PAWN:
                ke.image(isEnemy ? bp : wp, x, y, SQUARE_SIZE, SQUARE_SIZE);
                break;

            case BISHOP:
                ke.popMatrix();

          /*for(PVector fx : _particles)
          {
           int dist3 = PApplet.parseInt(sqrt(dist(fx.x, fx.y, x, y)));
           int a = PApplet.parseInt(dist3*9.5f);
           //noFill();
           fill(a,255-a,0, constrain(255-a, 0, 255));
           //strokeWeight(4);
           fx.x += PApplet.parseInt(cos(this.getScene().getSceneTime())*HALF_SQUARE_SIZE/100);
           ellipseMode(RADIUS);
           ellipse(fx.x+HALF_SQUARE_SIZE/2, fx.y+HALF_SQUARE_SIZE, dist3/1.2f, dist3/1.2f);
           //strokeWeight(1);
          }*/

                ke.pushMatrix();
                ke.scale(0.95f,0.95f);
                ke.translate(((DISPLAY_W*0.05f)/2),0,0);

                ke.image(bB, x, y, SQUARE_SIZE, SQUARE_SIZE);
                break;

            case KNIGHT:
            case ROOK:
                boolean isKnight = _displayType == PieceType.KNIGHT;
                float m = HALF_SQUARE_SIZE/4;

                if(targetx!=0 && targetx != x) {
                    float dist = abs(dist(x,0,targetx,0));
                    if(!isKnight) {
                        dist/=10;
                    } else {
                        dist/=6;
                    }

                    float sz = abs(1/norm(dist,1,SQUARE_SIZE))*2;
                    sz = constrain(sz, m, HALF_SQUARE_SIZE*2);
                    float a = constrain(200-sz,0,200);

                    //noStroke();
                    //fill(200,0,0,a);

                    //rect(targetx+(HALF_SQUARE_SIZE-sz/2)-1,y+(HALF_SQUARE_SIZE-sz/2)-1,sz+1,sz+1);
                }
                if(targety!=0 && targetx==0) {
                    float dist = abs(dist(0,y,0,targety));
                    if(!isKnight) {
                        dist/=10;
                    } else {
                        dist/=6;
                    }

                    float sz = abs(1/norm(dist,1,SQUARE_SIZE))*2;
                    sz = constrain(sz, m, HALF_SQUARE_SIZE*2);
                    float a = constrain(200-sz,0,200);

                    //noStroke();
                    //fill(200,0,0,a);

                    //rect(x+(HALF_SQUARE_SIZE-sz/2)-1,targety+(HALF_SQUARE_SIZE-sz/2)-1,sz+1,sz+1);
                }

                if(isKnight)
                {
                    if(magnify) {
                        float offs=SQUARE_SIZE/20;
                        ke.image(bN, x-offs, y-offs, SQUARE_SIZE+(offs*2), SQUARE_SIZE+(offs*2));
                    } else {
                        ke.image(bN, x, y, SQUARE_SIZE, SQUARE_SIZE);
                    }
                }
                else ke.image(bR, x, y, SQUARE_SIZE, SQUARE_SIZE);
                break;

            case QUEEN:
                ke.image(bQ, x, y, SQUARE_SIZE, SQUARE_SIZE);
                break;
        }

        // debug pt
        //popMatrix();
        ke.noStroke();
    /*fill(0,0,255);
    ellipse(x,y,10f,10f);

    // debug collision box
    noFill();
    strokeWeight(2);
    stroke(255,200,50);

    int offset = 0, square_offset = 0;
      offset = parseInt(HALF_SQUARE_SIZE / 1.8f);
      square_offset = offset*2;

    rect(x+offset,y+offset, SQUARE_SIZE-square_offset, SQUARE_SIZE-square_offset);
    noStroke();*/
        //pushMatrix();
    }


    public void onInit() {

    }

}
