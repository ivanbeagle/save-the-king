package com.beagleinteractive.savetheking;


import com.beagleinteractive.kingengine.IKRoutine;
import com.beagleinteractive.kingengine.KEntity;
import com.beagleinteractive.kingengine.KScene;
import processing.core.PVector;
import static com.beagleinteractive.kingengine.ThirdParty.*;


public class Explosion extends KEntity {
    // ---------------------------------------------------------------------------------------------
    // VARS
    // ---------------------------------------------------------------------------------------------
    public static final int END_EXPLOSION = 20;
    public static ImageSequence explog;

    GIFAnimation explosion;
    float x, y;
    int offset;


    // ---------------------------------------------------------------------------------------------
    // VARS
    // ---------------------------------------------------------------------------------------------


    /**
     * CTR
     * @param scene
     * @param x
     * @param y
     * @param col
     * @param reversed
     * @param event
     */
    public Explosion(KScene scene, float x, float y, int col, boolean reversed, final IKRoutine event) {
        super(scene);

        if(explog == null)
            explog = new ImageSequence("explosion_", ".png", 4, END_EXPLOSION, 1);

        this.x = x;
        this.y = y;

        final Explosion instance = this;

        explosion = new GIFAnimation(explog);
        explosion.reversed = reversed;
        explosion.tintCol = col;
        explosion.delay=0.01f;
        explosion.frameEvent = new IKRoutine<GIFAnimationEvt>() {
            public void doJob(GIFAnimationEvt e) {
                boolean reversed = e.animation.reversed;
                if((!reversed && e.frame == END_EXPLOSION-1) || (reversed && e.frame == 0)) {
                    e.animation.play=false;
                    instance.destroy();

                    if(event != null)
                        event.doJob(e);
                }
            }
        };
    }


    public void onRender() {
        explosion.play=GameFragment.stepGame;
        explosion.position = new PVector(x - GameFragment.HALF_SQUARE_SIZE, GameFragment.stepGame ? ++y - GameFragment.HALF_SQUARE_SIZE : y - GameFragment.HALF_SQUARE_SIZE);

        int sz = (int)(GameFragment.SQUARE_SIZE*2)+(offset);
        explosion.render(-offset,-offset, sz, sz);
    }


    public void onInit() {}
    public void onUpdate() {}
    public void onDestroy() {}
}