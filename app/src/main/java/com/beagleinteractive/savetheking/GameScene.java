package com.beagleinteractive.savetheking;


import com.beagleinteractive.kingengine.IKRoutine;
import com.beagleinteractive.kingengine.KScene;
import com.beagleinteractive.kingengine.KTimerJob;
import java.util.ArrayList;
import processing.core.PApplet;
import static com.beagleinteractive.kingengine.ThirdParty.*;
import static com.beagleinteractive.savetheking.GameFragment.*;


public class GameScene extends KScene {
    // ---------------------------------------------------------------------------------------------
    // VARS
    // ---------------------------------------------------------------------------------------------
    int camperingTimes;
    boolean castling;
    float[] e1, e2;

    KTimerJob gameTimer, dieTimer;
    ArrayList<GridObj> gameEntities;
    Piece king, oo1, oo2, oo3;

    private float _max;


    // ---------------------------------------------------------------------------------------------
    // API
    // ---------------------------------------------------------------------------------------------


    /**
     * CTR
     */
    public GameScene()
    {
        super();

        gameEntities = new ArrayList<GridObj>();

        _max = DISPLAY_W - SQUARE_SIZE;
        e1 = grid2world(E,1);
        e2 = grid2world(E,2);

        // instancing
        king = new Piece(this, Piece.PieceType.KING);
        king.isEnemy=false;
        king.x = e2[X]-HALF_SQUARE_SIZE;
        king.y = e1[Y];
    }


    public KTimerJob getDieTimer() {
        return dieTimer;
    }


    public void onInit() {
        ke.log("started game!");

        final KScene instance = this;

        isPlaying = true;

        // TODO, if retry restore score and level
        level = 2;
        score = 0;

        // config rooks map
        RooksMap.create();

        // destroy all pending timers
        ke.destroyPendingJobs();


        // run infinite timer for enemy generation
        IKRoutine gameRoutine = new IKRoutine<KScene>() {
            public void doJob(KScene scene) {
                if(!stepGame || tutorial || isGameOver)
                    return;

                score+=level;

                int t = (int)getSceneTime();
                boolean levelUp=false;

                if(t % (int)(level*5) == 0 && level < MAX_LEVEL) { // max level reached?
                    level+=1;
                    levelUp=true;
                }

                // max num of enemies
                if(gameEntities.size() >= MAX_ENEMIES) {
                    //ke.log("to many entities... skip");
                    return;
                }

                // check if king is on one of both bordelines
                boolean borderlineA = king.x <= SQUARE_SIZE;
                boolean borderlineB = king.x >= (G * SQUARE_SIZE);
                if(borderlineA || borderlineB)
                {
                    //ke.log("the king is campering now!");
                    if(camperingTimes > 1) {
                        //ke.log("bastard pawn spawned!");
                        camperingTimes = 0;
                        // instancing bastard pawn
                        Piece bastardPawn = new Piece(instance, Piece.PieceType.PAWN);
                        bastardPawn.setByGrid(borderlineA ? 0 : H, ROWS-2);

                        gameEntities.add(bastardPawn);

                        soundSpawner.play();
                    }
                    else camperingTimes++;
                } else camperingTimes=0;

                // max enemies to spawn? 2
                int spawn = constrain(level % 2, 1, 2);
                int i=0;
                //int rooks=0;

                // loop spawn
                Piece.PieceType lastKind = Piece.PieceType.KING;

                do {

                    // spawn enemies
                    int k = (int)ke.random(1,5);
                    int randx = (int)ke.random(0,H);

                    // powerup check, skip enemies if powerup
                    ke.randomSeed(ke.millis());
                    float rndp = ke.random(1,10000);
                    if(rndp > 1800 && rndp < 2000 && !castling && t%2 == 0) {
                        //ke.log("power up available!");
                        PowerUp pow = new PowerUp(instance);
                        pow.setByGrid(randx, ROWS-2);
                        gameEntities.add(pow);

                        soundSpawner.play();

                        break;
                    }

                    // avoid duplicates and a lot of rooks for time
                    Piece.PieceType kind = Piece.PieceType.values()[k];
                    if(kind == lastKind)
                        continue;

                    // specialize the rook spawn algo
                    if(kind == Piece.PieceType.ROOK) {

                        int nrooks = RooksMap.rooksUnder(HALF_DISPLAY_H-SQUARE_SIZE*2);
                        //ke.log("rooks: " + nrooks);

                        if(nrooks >= 1) {
                            //ke.log("skip spawn; to many rooks: " + nrooks);
                            continue;
                        }
                    }

                    // early queen some time...
                    int rnd = PApplet.parseInt(ke.random(1,200));
                    if(rnd % 2 == 0 && rnd > 186 && t%2==0) {
                        kind = Piece.PieceType.QUEEN;

                        soundSpawner.play();
                    }

                    lastKind = kind;

                    // instancing
                    Piece enemy = new Piece(instance, kind);
                    enemy.setByGrid(randx, ROWS-2);

                    gameEntities.add(enemy);

                    i++;
                }
                while(i < spawn);
            }
        };

        // loop timer
        gameTimer = ke.startJob(gameRoutine, 2, true);
    }


    public void onUpdate() {
        if(isGameOver) {
            //stepGame=false;

            //soundLoop.fadeOut(_gDeltaTime/2);
            return;
        }


        if(tutorial) {
            float t = getSceneTime();
            if(t > 12 || (ke.mousePressed && abs(ke.mouseX-ke.pmouseX) > HALF_SQUARE_SIZE/10)) {
                tutorial = false;

                if(ke.mousePressed)
                    reminderTutorial=false;

                // save cfg
                json.setBoolean("reminderTutorial", reminderTutorial);
                ke.saveJSONObject(json, "savetheking.json");

            }
            return;
        }

        // is pause?
        if(!stepGame)
            return;

        // score update
        //score+=1;

        // entities checker+collision
        ArrayList<GridObj> toRemove = new ArrayList();

        for(GridObj e : gameEntities)
        {
            boolean isPowerUp = e instanceof PowerUp;

            if(e.isDirty()) // is off of screen or deleted?
                toRemove.add(e);
            else
            {
                // check for collision if castling
                if(!isPowerUp && castling && (e.collide(oo1) || e.collide(oo2) || e.collide(oo3)))
                {
                    toRemove.add(e);
                    destroyCastling();

                    soundExplo.play();
                } // endif

                if(e.collide(king) && !e.die)
                {
                    if(isPowerUp && !castling)
                    {
                        castling(); //o-o-o
                        e.die=true; // destroy the powerup with ease fx

                        soundPower.play();

                    } else {
                        // mark all objects as destroyed
                        toRemove.clear();
                        toRemove.addAll(gameEntities);

                        if(!isGameOver) {
                            // remove o-o-o
                            destroyCastling();

                            // set flags
                            king.die=true;
                            isGameOver=true;

                            // play sound
                            soundGameOver.play();

                            // start die timer
                            IKRoutine dieRoutine = new IKRoutine<KScene>() {
                                public void doJob(KScene scene) {

                                    ke.destroyPendingJobs();
                                    ke.setScene(new GameOverScene());

                                }};
                            dieTimer = ke.startJob(dieRoutine, 2, false);
                        } // end of game over block

                    }
                } // endif

            }
        }

        // free objects to destroy
        for(GridObj e : toRemove) {
            gameEntities.remove(e);
            e.destroy();
        }

        // castling update
        if(castling) {
            oo1.x = king.x - SQUARE_SIZE;
            oo2.x = king.x;
            oo3.x = king.x + SQUARE_SIZE;
        }

        // drag update
        king.magnify = false;

        // drag
        if(ke.mousePressed) {
            float destX = king.x + constrain(ke.mouseX-ke.pmouseX, -SQUARE_SIZE, SQUARE_SIZE);
            if(destX != 0) {
                if(destX > _max)
                    destX = _max;
                if(destX < 0)
                    destX = 0;

                king.x = destX;
            }
            float mdist = abs(ke.mouseX - king.x - HALF_SQUARE_SIZE);
            if(mdist < SQUARE_SIZE)
            {
                king.magnify = true;
            }
        }

    }


    public void castling() {
        if(castling)
            return;

        oo1=new Piece(this, Piece.PieceType.PAWN);
        oo1.isEnemy=false;
        oo1.x=king.x - SQUARE_SIZE;
        oo1.y = e2[Y];

        oo2=new Piece(this, Piece.PieceType.PAWN);
        oo2.isEnemy=false;
        oo2.x=king.x;
        oo2.y = e2[Y];

        oo3=new Piece(this, Piece.PieceType.PAWN);
        oo3.isEnemy=false;
        oo3.x=king.x + SQUARE_SIZE;
        oo3.y = e2[Y];

        float offs = HALF_SQUARE_SIZE / 4;

        new Explosion(this, oo1.x, oo1.y-offs, ke.color(255,255,255,255), true, null).offset=10;
        new Explosion(this, oo2.x, oo2.y-offs, ke.color(255,255,255,255), true, null).offset=10;
        new Explosion(this, oo3.x, oo3.y-offs, ke.color(255,255,255,255), true, null).offset=10;

        castling = true;
    }


    public void destroyCastling() {
        if(!castling)
            return;

        oo1.destroy();
        oo2.destroy();
        oo3.destroy();

        castling = false;
    }


    // --------------------------------------------------------------------------------------


    public void onPreRender() {
        //if(aspect < 0.6) {
        ke.pushMatrix();
        ke.scale(0.95f, 0.95f, 0);
        ke.translate(((DISPLAY_W*0.05f)/2),0,0);
        //}

        ke.stroke(0);
        ke.strokeWeight(1);

        if(isGameOver) {
            float timeDiff = (dieTimer.getElapsedTime() - ke.getTime());
            float fade = (timeDiff / dieTimer.getDelayInSec()) * 255;
            ke.tint(ke.color(255,fade,fade,fade));
        }

        renderCheckboard();
    }


    public void onPostRender() {
        ke.stroke(0);
        ke.strokeWeight(1);
        renderBorders();
        renderCaptions(); // score & level

        float offset = SQUARE_SIZE*2;
        ke.image(gradmon, 0, ((GAMEVIEW_H+SQUARE_SIZE)-offset), DISPLAY_W, (offset));

        if(tutorial && reminderTutorial)
        {
            ke.pushMatrix();
            ke.translate(0,-HALF_SQUARE_SIZE,0);

            float sin = sin(ke.getTime());
            float delay = 12-getSceneTime();
            int ndelay = PApplet.parseInt(delay);

            ke.pushMatrix();
            ke.translate(0.1f*HALF_DISPLAY_W,-SQUARE_SIZE);
            ke.scale(0.9f,0.9f);

            float t = getSceneTime();
            ke.image(boxhelp, 0, parseInt(HALF_DISPLAY_H-SQUARE_SIZE*3), parseInt(DISPLAY_W), parseInt(SQUARE_SIZE*4));//, color(10,10,50,200), color(0,0,10,250), Y_AXIS);

            if(t-((int)t) > 0.25f) {
                ke.textSize(HALF_SQUARE_SIZE*0.8f);
                drawTextShadow("Drag now to skip this tutorial!",ke.color(50,255,50),50,parseInt(HALF_DISPLAY_W-ke.textWidth("Drag now to skip this tutorial!")/2), parseInt(HALF_DISPLAY_H + HALF_SQUARE_SIZE),1,4);
            }

            ke.textSize(HALF_SQUARE_SIZE*1.2f);
            drawTextShadow("DRAG TO MOVE THE KING",ke.color(150,150,255),10,parseInt(HALF_DISPLAY_W-ke.textWidth("DRAG TO MOVE THE KING")/2), parseInt(HALF_DISPLAY_H - SQUARE_SIZE),1,4);

            ke.textSize(HALF_SQUARE_SIZE);
            drawText("Autostart in: ", 200, "" + parseInt(delay) , ke.color(50,255,50), parseInt(HALF_DISPLAY_W-ke.textWidth("Autostart in: XX")/2), parseInt(HALF_DISPLAY_H - HALF_SQUARE_SIZE));
            ke.popMatrix();

            arrow.render(0, parseInt(e1[Y]-HALF_SQUARE_SIZE),parseInt(SQUARE_SIZE*2), parseInt(SQUARE_SIZE));
            ke.pushMatrix();
            ke.scale(-1,1);
            arrow.render(parseInt(-DISPLAY_W), parseInt(e1[Y]-HALF_SQUARE_SIZE), parseInt(SQUARE_SIZE*2), parseInt(SQUARE_SIZE));
            ke.popMatrix();

            ke.image(hand, king.x+(cos(ke.getTime())*SQUARE_SIZE*3)/*lerpx*/, king.y - SQUARE_SIZE-HALF_SQUARE_SIZE /*lerpy*/, SQUARE_SIZE, SQUARE_SIZE*1.2f);
            ke.popMatrix();
        }

        if(isGameOver) {
            ke.noTint();
        }

        // aspect
        ke.popMatrix();
    }


    private void renderCheckboard() {
        if(checkboard != null) {
            /*ke.clear();

            ke.fill(0);
            ke.rect(0,0,DISPLAY_W, DISPLAY_H);*/
            ke.background(0);

            ke.image(checkboard,0,SQUARE_SIZE); // offset from score is square size
            return;
        }

        checkboard = ke.createGraphics(DISPLAY_W, GAMEVIEW_H);
        checkboard.beginDraw();

        int col = 255;

        for(int i=0; i<8; i++)
        {
            for(int j=0; j < ROWS; j++)
            {
                //fill((j+i) % 2 == 0 ? 50 : 250);
                int cr1 = (j+i) % 2 == 0 ? 250 - (j*4) : 50 - (j*2);
                int cr2 = parseInt(cr1/(1.5f));
                int c1 = ke.color(cr1,cr1,cr1);
                int c2 = ke.color(cr2,cr2,cr2);

                linearGradient(parseInt(i*SQUARE_SIZE), parseInt(j*SQUARE_SIZE), SQUARE_SIZE, SQUARE_SIZE, c1, c2, Y_AXIS, checkboard);
            }
        }

        int offset = parseInt(SQUARE_SIZE*4);

        //checkboard.image(gradmon, 0, (GAMEVIEW_H-offset), GAMEVIEW_W, (offset));

        checkboard.endDraw();
    }


    private void renderBorders() {
        ke.fill(0);
        ke.rect(0,0, DISPLAY_W, SQUARE_SIZE);
    }


    private void renderCaptions() {
        int barY = parseInt(HALF_SQUARE_SIZE+10);
        int textSz = parseInt(HALF_SQUARE_SIZE);

        ke.textSize(textSz);
        drawText("Score: ", 255, ""+ (int)score, ke.color(50,255,50), 10, barY);
        drawText("Level: ", 255, ""+ ((int)level-1), ke.color(50,255,50), HALF_DISPLAY_W, barY);

        if(!stepGame) {
            ke.fill(255,0,0);
            ke.text("PAUSE", HALF_DISPLAY_W-(textSz+textSz/2), HALF_DISPLAY_H);
        }
    }
}
