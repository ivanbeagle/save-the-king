package com.beagleinteractive.savetheking;


import com.beagleinteractive.kingengine.KScene;
import static com.beagleinteractive.kingengine.ThirdParty.*;
import static com.beagleinteractive.savetheking.GameFragment.*;


public class GameOverScene extends KScene {
    // ---------------------------------------------------------------------------------------------
    // VARS
    // ---------------------------------------------------------------------------------------------
    boolean isRecord;


    // ---------------------------------------------------------------------------------------------
    // API
    // ---------------------------------------------------------------------------------------------


    public void onInit() {
        ke.log("game over!");

        isPlaying=false;

        isRecord = score > localRecord;
        if(isRecord)
        {
            localRecord = score;

            // save record
            json.setInt("score",localRecord);
            json.setInt("playTimes", playTimes);

            ke.saveJSONObject(GameFragment.json, "savetheking.json");
        }
    }


    public void onUpdate() {
        float t = getSceneTime();

        if(t > 8 || (ke.mousePressed && t > 1)) {
            ke.setScene(new MenuScene());
        }
    }


    public void onPostRender() {
        renderMenu();
    }


    private void renderMenu() {
        ke.image(gameOver, 0, 0, DISPLAY_W, DISPLAY_H);

        ke.textSize(SQUARE_SIZE);

        float t = getSceneTime();
        int yOff = parseInt(HALF_SQUARE_SIZE/4);


        if(t-((int)t) > 0.25f) {
            int x = parseInt(HALF_DISPLAY_W);
            int y = parseInt(DISPLAY_H - (SQUARE_SIZE*2)-(HALF_SQUARE_SIZE));
            int col = 200;


            if(isRecord)
            {
                int sin = parseInt(sin(ke.getTime())*10);
                ke.imageMode(CENTER);
                ke.image(record, x,y-parseInt(SQUARE_SIZE/10), parseInt(DISPLAY_W - HALF_DISPLAY_W/10)+sin, parseInt(SQUARE_SIZE*2 + HALF_SQUARE_SIZE)+sin);
                ke.imageMode(CORNER);
                col = ke.color(0,80,0);
                drawTextShadow(""+score, col, 10,  parseInt(HALF_DISPLAY_W - ke.textWidth(""+score)/2), y+parseInt(HALF_SQUARE_SIZE)+yOff, 1, 3);
            }
            else
                drawTextShadow(""+score, col, 10,  parseInt(HALF_DISPLAY_W - ke.textWidth(""+score)/2), y+parseInt(HALF_SQUARE_SIZE)+yOff, 1, 3);

        }

        ke.textSize(HALF_SQUARE_SIZE);
        drawTextShadow(""+localRecord, 100, 10,  parseInt(HALF_DISPLAY_W - ke.textWidth(""+localRecord)/2), DISPLAY_H - parseInt(HALF_SQUARE_SIZE-HALF_SQUARE_SIZE/10)+yOff, 1, 3);
    }
}