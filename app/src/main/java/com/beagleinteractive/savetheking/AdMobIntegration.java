package com.beagleinteractive.savetheking;


import android.content.res.Resources;
import android.os.Handler;
import android.widget.Toast;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import static com.beagleinteractive.savetheking.GameFragment.*;


public class AdMobIntegration {
    // ---------------------------------------------------------------------------------------------
    // VARS
    // ---------------------------------------------------------------------------------------------
    public static AdView adView;
    public static InterstitialAd adFull;

    private static boolean _init;
    private static boolean _fullAdShow;


    // ---------------------------------------------------------------------------------------------
    // API
    // ---------------------------------------------------------------------------------------------


    public static boolean isInitialized() { return _init; }


    public static boolean isFullAdShow() { return _fullAdShow; }


    public static void init(final GameActivity activity) {
        final Resources res = activity.getResources();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if(BuildConfig.DEBUG)
                            Toast.makeText(activity, "Init AdMob in Debug mode, this will not come out in Release mode!", Toast.LENGTH_LONG).show();

                        try
                        {
                            adView = activity.findViewById(R.id.adView);

                            // init AdMob
                            MobileAds.initialize(activity, res.getString(R.string.admob_pub_id));

                            // banner
                            AdRequest adRequest = getNextAdv();
                            if (adRequest != null)
                                adView.loadAd(adRequest);

                            // inter-fullscreen
                            adFull = new InterstitialAd(activity);
                            adFull.setAdUnitId(res.getString(R.string.admob_inter_id));
                            adFull.setAdListener(getAdFullListener(activity.game));
                            AdRequest adRequest2 = getNextAdv();
                            if (adRequest2 != null)
                                adFull.loadAd(adRequest2);

                            _init=true;
                        }
                        catch (Exception ex) {
                            if(BuildConfig.DEBUG)
                                Toast.makeText(activity, "Init AdMob in Debug mode: FAIL", Toast.LENGTH_SHORT).show();

                            println(ex.getMessage());
                            ex.printStackTrace();
                            _init=false;
                        }
                    }
                });
            }
        }, 1300);
    }


    // ---------------------------------------------------------------------------------------------
    // code
    // ---------------------------------------------------------------------------------------------


    private static AdListener getAdFullListener(final GameFragment game) {
        return new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
                try
                {
                    // load next ad
                    adFull.loadAd(getNextAdv());
                }
                catch (Exception ex)
                {
                    println(ex.getMessage());
                    ex.printStackTrace();
                }
                finally {
                    _fullAdShow=false;
                    //game.setScene(new GameScene());
                }
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
                _fullAdShow=true;
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the interstitial ad is closed.
                try
                {
                    // load next ad
                    adFull.loadAd(getNextAdv());
                }
                catch (Exception ex)
                {
                    println(ex.getMessage());
                    ex.printStackTrace();
                }
                finally {
                    _fullAdShow=false;
                    game.setScene(new GameScene());
                }
            }
        };
    }


    private static AdRequest getNextAdv() {
        try
        {
            // change YOUR_DEVICE_ID with your Device ID code            
            if (BuildConfig.DEBUG) {
                return new AdRequest.Builder().addTestDevice("YOUR_DEVICE_ID").build();
            }

            return new AdRequest.Builder().build();
        }
        catch (Exception ex) {
            println(ex.getMessage());
            ex.printStackTrace();

            // retry again
            try { return new AdRequest.Builder().build(); } catch(Exception ex2) { return null; }
        }
    }
}
