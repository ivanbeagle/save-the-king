package com.beagleinteractive.savetheking;


import java.util.ArrayList;


public class RooksMap
{
    // ---------------------------------------------------------------------------------------------
    // VARS
    // ---------------------------------------------------------------------------------------------
    private static ArrayList<Piece> _map;


    // ---------------------------------------------------------------------------------------------
    // API
    // ---------------------------------------------------------------------------------------------


    public static void create()
    {
        if(_map != null) {
            _map.clear();
            _map = null;
        }
        _map = new ArrayList<Piece>();
    }


    public static void ensure(Piece rook) {
        if(_map.contains(rook))
            return;

        _map.add(rook);
    }


    public static void remove(Piece rook) {
        if(_map.contains(rook))
            _map.remove(rook);
    }


    /*public static int rooksOnRow(int row) {
        int n = 0;

        for(Piece r : _map) {
            int ry = GameFragment.world2grid(0,r.y)[GameFragment.Y];
            if(ry == row)
                n++;
        }

        return n;
    }*/


    public static int rooksUnder(float threshold) {
        //ke.log("checking rook map: " + _map.size());

        int n = 0;

        for(Piece r : _map) {
            float ry = r.y;
            if(ry < threshold) {
                n++;
            }
        }

        return n;
    }
}
